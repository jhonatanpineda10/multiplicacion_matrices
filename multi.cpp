#include <iostream>
#include <vector>
#include <iomanip>
#include <limits>
#include <algorithm>

using namespace std;

const int g = numeric_limits<int>::max(); // Infinito

// imprime la matriz
void print(const vector<vector<int>>& C){
    for ( int i = 0; i < C.size(); i++ ) {
        for ( int j = 0; j < C[0].size(); j++ )
          cout<< setw ( 10 ) << C[i][j] <<' ';
        cout<<'\n';
        }
}

int sum(int a, int b){
    if(a == g || b == g) return g;
    else return a+b;

}
// copia la matriz A en C
/*void copy(const vector<vector<int>>& A, vector<vector<int>>& C){
    for(int i=0;i<A.size();i++){
      for(int j=0;j<A[0].size();j++){
        C[i][j]=A[i][j];
      }
    }
}
*/
// Se genera la matriz identidad en C
void identidad(const vector<vector<int>>& A, vector<vector<int>>& C){
    for(int i=0;i<A.size();i++){
        for(int j=0;j<A[0].size();j++){
            if (i == j) C[i][j] = 1;
            else C[i][j] = 0;
        } 
    } 
}

// Calcula el diametro del grafo
int Diametro(const vector<vector<int>>& C){
    int max = 0;
    for(int i=0;i < C.size(); i++){
         int maxv = *max_element(C[i].begin(), C[i].end());
         if (maxv >= max && maxv != g) max = maxv ;
  }
    return max;
}
// multiplicacion A*B y el resultado se guarda en C
void matrixmult(const vector<vector<int>>& A, const vector<vector<int>>& B,vector<vector<int>>& C){
    int rows_A = A.size();
    int cols_A = A[0].size();
    int rows_B = B.size();
    int cols_B = B[0].size();

    // Se llena la matriz C con infinito
    for(int y=0;y<rows_A;y++){
        for(int x=0;x<cols_B;x++){
            C[y][x]=g;
        }
    }

    // se realiza la multiplicacion "rara"    
    for(int i=0;i<rows_A;i++){
        for(int j=0;j<cols_B;j++){
            for(int k=0;k<cols_A;k++)
             C[i][j]=min(C[i][j], sum(A[i][k], B[k][j]));
        }
    }
}

// Funcion extraña para multiplicar A^n
void strange(const vector<vector<int>>& A,vector<vector<int>>& C){
    vector< vector<int>> J ( A.size(), vector<int> (A.size()) );
    C = A;
    for (int i = 0; i < A.size(); i++){
         matrixmult(A,C,J);
         C = J;
    }
}

// Funcion extraña para multiplicar A^(a cualquier exponente)
void strangeN(const vector<vector<int>>& A,vector<vector<int>>& C,int n){
    vector< vector<int>> J ( A.size(), vector<int> (A.size()) );
    C = A;
    for (int i = 0; i < n; i++){
         matrixmult(A,C,J);
         C = J;
    }
}

// Funcion extraña2 para multiplicar A^n complejidad logaritmica
void strange2(const vector<vector<int>>& A,vector<vector<int>>& C){
    vector< vector<int>> J ( A.size(), vector<int> (A.size()) );
    vector< vector<int>> K ( A.size(), vector<int> (A.size()) );
    if (A.size() == 0){
            //identidad
            identidad(A,C);
        }
        else {
            if (A.size() == 1){
            // A
            C = A;
        }
        else{
            if(A.size()%2 == 0){
                // (A*A)^(n/2)
                matrixmult(A,A,J);
                strangeN(J,C,A.size()/2);
            }
            else{
            // A*(A*A)^((n-1)/2)
                matrixmult(A,A,J);
                strangeN(J,K,(A.size()-1)/2);
                matrixmult(A,K,C);
            }
            }
    }
} 

int main() {
  vector< vector<int> > A = {
  {0, 1, 3, g, g, g, g, g},
  {5, 0, 1, 8, g, g, g, g},
  {g, 9, 0, g, 8, g, g, g},
  {g, g, g, 0, g, g, g, g},
  {g, g, 7, g, 0, g, 2, 7},
  {g, 1, g, 4, g, 0, 7, g},
  {g, g, 7, g, g, g, 0, g},
  {g, g, g, g, g, 1, g, 0}};

  /*vector< vector<int> > A = {
  {0, 1, 3, g, g, g, g, g,g},
  {5, 0, 1, 8, g, g, g, g,g},
  {g, 9, 0, g, 8, g, g, g,g},
  {g, g, g, 0, g, g, g, g,g},
  {g, g, 7, g, 0, g, 2, 7,g},
  {g, 1, g, 4, g, 0, 7, g,g},
  {g, g, 7, g, g, g, 0, g,g},
  {g, g, g, g, g, 1, g, 0,g},
  {g, g, g, g, g, 1, g, g,0}};*/

  vector< vector<int>> C ( A.size(), vector<int> (A.size()) );
  
  cout<<endl <<"fase 1 "<<endl<<endl;
  strange(A,C);
  print(C);
  int D = Diametro(C);
  cout<<endl <<"El Diametro es: "<< D <<endl;

  cout<<endl <<"fase 2 "<<endl<<endl;
  strange2(A,C);
  print(C);
  D = Diametro(C);
  cout<<endl <<"El Diametro es: "<< D <<endl;
  return 0;
}

//g++ -std=c++11 -o mult multi.cpp

//http://www.dis.uniroma1.it/challenge9/download.shtml